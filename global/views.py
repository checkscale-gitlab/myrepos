from django.shortcuts import redirect, render
from django.http import HttpResponse


def homepage_view(request):
    """
    Method responsible for loading the homepage.
    """

    if request.user.is_authenticated:
        return redirect(
            '/user/{username}'.format(
                username=request.user.username
            )
        )
    else:
        return render(request, 'homepage.html')
