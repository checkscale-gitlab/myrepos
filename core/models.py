from django.db import models
from django.contrib.auth.models import User

import json


class Repository(models.Model):
    """
    Class that represents the repository.
    """

    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='repositories',
        blank=True,
        null=True
    )
    name = models.CharField(
        max_length=250,
        null=True,
        blank=True
    )
    full_name = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    is_private = models.BooleanField(
        null=True,
        blank=True
    )
    is_fork = models.BooleanField(
        null=True,
        blank=True
    )
    description = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    language = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    html_url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    user_commits = models.IntegerField(
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = "Repository"
        verbose_name_plural = "Repositories"

    def __str__(self):
        return self.full_name


class Tag(models.Model):
    """
    Class that represents the tag.
    """

    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='tags',
        blank=True,
        null=True
    )
    repository = models.ForeignKey(
        Repository,
        on_delete=models.CASCADE,
        related_name='tags',
        blank=True,
        null=True
    )
    text = models.CharField(
        max_length=250,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __str__(self):
        return self.repository.full_name + " - " + \
            self.text
